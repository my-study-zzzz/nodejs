var express = require('express');
var http = require('http');
var static = require('serve-static');
var path = require('path');

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
//파일 업로드용 미들웨어
var multer = require('multer');
var fs = require('fs');

// 클라이언트에서 ajax를 요청했을 때 cors(다중서버접속) 지원
var cors = require('cors');




var app = express();

app.set('port', process.env.PORT || 3000);
app.use('/public',static(path.join(__dirname, 'public')));
app.use('/uploads', static(path.join(__dirname, 'uploads')));


app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(cors());

var storage = multer.diskStorage({
    destination: function(req, file, callback){
        callback(null, 'uploads');
    }, 
    filename: function(req, file, callback){
        //callback(null, file.originalname + Date.now());

        var extension = path.extname(file.originalname);
        var basename = path.basename(file.originalname, extension);
        callback(null, basename + Date.now() + extension);
    }
});

var upload = multer({
    storage: storage,
    limits:{
        files:10,
        fileSize:1024*1024*1024
    }
});

var router = express.Router();

router.route('/process/photo').post(upload.array('photo', 1), 
function(req,res){
    console.log('/process/photo 라우팅 함수 호출됨.');

    var files = req.files;

    var originalname;
    var filename;
    var mimetype;
    var size;

    if(Array.isArray(files)){
        for(var i = 0; i < files.length; i++){
            originalname = files[i].originalname;
            filename = files[i].filename;
            mimetype = files[i].mimetype;
            size = files[i].size;

        }
    }

    res.writeHead(200, {"Content-Type" : "text/html;charset=utf-8"});
    res.write("<h1>파일 업로드 성공</h1>");
    res.write("<p>원본파일 : "+ originalname +"</p>");
    res.write("<p>저장파일 : "+ filename +"</p>");
    res.end();
});

router.route('/process/memo').post(upload.array('photo', 1), function(req,res){

    var files = req.files;

    var originalname;
    var filename;
    var mimetype;
    var size;

    if(Array.isArray(files)){
        for(var i = 0; i < files.length; i++){
            originalname = files[i].originalname;
            filename = files[i].filename;
            mimetype = files[i].mimetype;
            size = files[i].size;

        }
    }

    if(req.body.writer){
        //res.redirect('/public/memoSaved.html');
        res.writeHead(200, {"Content-Type" : "text/html;charset=utf-8"});
        res.write("<h1>메모가 저장되었습니다.</h1>");
        res.write("<p>원본파일 : "+ originalname +"</p>");
        res.write("<p>저장파일 : "+ filename +"</p>");
        res.write(`<button type='button' onclick='location.href="/process/memo"'>다시작성</button>`);
        res.end();
    } 
});

router.route('/process/memo').get(function(req,res){
    res.redirect('/public/memo.html');
});

app.use('/', router);

var server = http.createServer(app).listen(app.get('port'), function(){
    console.log('익스프레스로 웹 서버를 실행함 : ' + app.get('port'));
});






