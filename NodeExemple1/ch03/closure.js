function outter(){
    
    var count = 0;
    
    var inner = function(){
        console.log(count++);
    }

    return inner;
}

var history = outter();

history();
history();
history();