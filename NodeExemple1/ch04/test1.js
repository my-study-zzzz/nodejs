var url = require('url');

// 주소 문자열 -> URL 객체
var curURL = url.parse('https://search.naver.com/search.naver?sm=top_hty&fbm=0&ie=utf8&query=steve+jobs');

// URL 객체 -> 주소 문자열
var curStr = url.format(curURL);

console.log(curURL);
console.log('주소 문자열 : %s', curStr);

var queryString = require('querystring');
var param = queryString.parse(curURL.query);

console.log('요청 파라미터 중 query의 값 : ', param.query);
console.log('원본 요청 파라미터 : ', queryString.stringify(param));